﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Our Base class where we can create children to inherit from
public class EngineBase : MonoBehaviour
{
    // acceleration indicates how fast the enemy accelerates
    [SerializeField]
    protected float acceleration = 5000f;

    // local references
    protected Rigidbody2D ourRigidbody;

    public void Start()
    {
        // populate ourRigidbody
        ourRigidbody = GetComponent<Rigidbody2D>();
    }
    // Accelerate takes a direction as a parameter, and applies a force in this provided direction
    // to ourRigidbody, based on the acceleration variables and the delta time.
    public virtual void Accelerate(Vector2 direction)
    {
        //calculate our force to add
        Vector2 forceToAdd = direction * acceleration * Time.deltaTime;
        // apply forceToAdd to ourRigidbody
        ourRigidbody.AddForce(forceToAdd);
    }
}
