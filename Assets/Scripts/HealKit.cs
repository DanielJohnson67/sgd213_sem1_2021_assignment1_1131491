using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealKit : MonoBehaviour
{
    // make the healingAmount a pseudo-public variable
    [SerializeField]
    public int healingAmount;
    // when we trigger with a collider
    void OnTriggerEnter2D(Collider2D col)
    {
        // if the game object is the player
        if (col.gameObject.tag == "Player")
        {
           
            // set the player to the game object
           GameObject player = col.gameObject;
            // trigger the HealthPickup code
           HealthPickup(player);
        }
    }
    // what happens when it collides with the player
    void OnCollisionEnter2D(Collision2D col)
    {
        // if the gameobject is the player
        
        if (col.gameObject.tag == "Player")
        {
            
            // set the player to the game object
           GameObject player = col.gameObject;
            // trigger the HealthPickup code
            HealthPickup(player);
        }
    }
    // HealthPickup handles the healing after a player has collided with the healthkit.
    // By grabbing the players health component and triggering the healscript that it owns
    private void HealthPickup(GameObject player)
    {
        // get the PlayersHealth from the player
       PlayerHealth playerHealth = player.GetComponent<PlayerHealth>();
        // check to see if the player has a health component
        if (playerHealth == null)
        {
            Debug.LogError("Player doesn't have a PlayerHealth component.");
            return;
        }
        else
        {
            // tell the playerHealth to initiate the heal
            playerHealth.Heal(healingAmount);
            Debug.LogError("Congratulations on healing.");
        }
    }

}