using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour, IHealth
{
    // set our initial values of health and our healthbar to the prefabs healthbar
    [SerializeField]
    protected int currentHealth;
    public int CurrentHealth { get { return currentHealth; } }

    [SerializeField]
    protected int maxHealth;
    public int MaxHealth { get { return maxHealth; } }

    [SerializeField]
    private Slider sldEnemyHealth;

    void Start()
    {
        currentHealth = maxHealth;
    }

    // <summary>
    /// Heal handles the functionality of receiving health
    /// </summary>
    /// <param name="healingAmount">The amount of health to gain, this value should be positive</param>
    public void Heal(int healingAmount)
    {
        // heal for the amount but if our new health is higher than our set max health set our current hp to the max
        currentHealth += healingAmount;
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    /// <summary>
    /// TakeDamage handles the functionality for taking damage
    /// </summary>
    /// <param name="damageAmount">The amount of damage to lose, this value should be positive</param>
    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;
        // call the health update script
        UpdateEnemyHealthSlider((float)currentHealth / (float)maxHealth);
        // if the object dont have enough health, die
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }
    // Update our healthbar slider to our new health as a percentage of our max health
    public void UpdateEnemyHealthSlider(float percentage)
    {
        sldEnemyHealth.value = percentage;

    }
        /// <summary>
        /// Handles all functionality related to when health reaches or goes below zero, should perform all necessary cleanup.
        /// </summary>
        public void Die()
    {
        //if we are part of the pool return
        if (GetComponent<PoolObject>() != null)
        {
            PoolManager.instance.ReturnObjectToPool(gameObject);
        }

        else
        {
            // destroy the object
            Destroy(gameObject);
        }
    }
}
