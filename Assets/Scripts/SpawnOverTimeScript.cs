﻿using UnityEngine;
using System.Collections;
// base parent class to spawn objects over time
public class SpawnOverTimeScript : MonoBehaviour
{
    // variable for our game object
    [SerializeField]
    public GameObject spawnObject;

    // Delay before starting to spawn
    [SerializeField]
    public float initialDelay = 2f;

    // Delay between spawns
    [SerializeField]
    public float spawnDelay = 2f;

    // variable for our renderer
    private Renderer ourRenderer;

    // Use this for initialization
    public void Start()
    {
        // get the renderer component
        ourRenderer = GetComponent<Renderer>();

        // Stop our Spawner from being visible!
        ourRenderer.enabled = false;

        // Call the given function after initialDelay seconds, 
        // and then repeatedly call it after spawnDelay seconds.
        InvokeRepeating("Spawn", initialDelay, spawnDelay);
    }
    //spawn logic
    public void Spawn()
    {
        string objectName = spawnObject.name;
        Debug.Log(objectName);
        // create two floats of a certain potential spawn range
        float X = transform.position.x - ourRenderer.bounds.size.x / 2;
        float XX = transform.position.x + ourRenderer.bounds.size.x / 2;

        // Randomly pick a point within the spawn object
        Vector2 SPAWNLOC = new Vector2(Random.Range(X, XX), transform.position.y);

 /*
         //While attemping to make the spawner use object pooling i couldnt get it to spawn each script on its timer sometimes it would say it spawned an object yet it never appears on screen
                if (objectName == "EnemyBoss")
                {
                    GameObject EnemyBoss = PoolManager.instance.GetObjectFromPool("EnemyBoss", SPAWNLOC, Quaternion.identity);

                    EnemyBoss.SetActive(true);
                }
                else if (objectName == "EnemySpaceShip")
                {
                    GameObject EnemySpaceShip = PoolManager.instance.GetObjectFromPool("EnemySpaceShip", SPAWNLOC, Quaternion.identity);

                    EnemySpaceShip.SetActive(true);
                }
                else if (objectName == "enemy")
                {
                    GameObject enemy = PoolManager.instance.GetObjectFromPool("enemy", SPAWNLOC, Quaternion.identity);

                    enemy.SetActive(true);
                }

                //else Instantiate(spawnObject, SPAWNLOC, Quaternion.identity);
                else if (objectName == "HealthPickup")
                {
                    GameObject HealthPickup = PoolManager.instance.GetObjectFromPool("HealthPickup", SPAWNLOC, Quaternion.identity);

                    HealthPickup.SetActive(true);
                }
                else if (objectName == "Pickup")
                {
                    GameObject Pickup = PoolManager.instance.GetObjectFromPool("Pickup", SPAWNLOC, Quaternion.identity);

                    Pickup.SetActive(true);
                }
                */
        // Spawn the object at the 'spawnLocation' position
        //else Debug.Log("Not Spawning any named object");
        Instantiate(spawnObject, SPAWNLOC, Quaternion.identity);
    }
}
