﻿using UnityEngine;
using System.Collections;

/// <summary>
/// PlayerMovementScript is inheriting from our EngineBase movement and we can specify which parts we want to change in the child class 
/// </summary>
public class PlayerMovement : EngineBase
{
    public void Accelerate(float horizontalInput)
    {
        // a horizontalInput of 0 has no effect, as we want our ship to drift
        if (horizontalInput != 0)
        {
            //calculate our force to add
            Accelerate(Vector2.right * horizontalInput);
        }
    }
    public override void Accelerate(Vector2 direction)
    {
        if (direction.magnitude != 0)
        {
            base.Accelerate(direction);
        }
    }
}
