﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField]
    private Slider sldPlayerHealth;

    // Check to see if we have more than 1 instance in our game
    void Start()
    {
        if (instance != null) {
            Debug.LogError("There is more than one UIManager in the scene, this will break the Singleton pattern.");
        }
        instance = this;
    }
    // Update the players health slidebar when called
    public void UpdatePlayerHealthSlider(float percentage) {
        sldPlayerHealth.value = percentage;
    }
}
