﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player Heal script handles the players IHealth component, by handling the setting of health, the healing, the taking of damage and the eventual death of the Player
public class PlayerHealth : MonoBehaviour, IHealth
{
    // Set our inital variables
    [SerializeField]
    protected int currentHealth;
    public int CurrentHealth { get { return currentHealth; } }

    [SerializeField]
    protected int maxHealth;
    public int MaxHealth { get { return maxHealth; } }

    // Set our current health to our maximum
    void Start()
    {
        currentHealth = maxHealth;
    }

    // <summary>
    /// Heal handles the functionality of receiving health
    /// </summary>
    /// <param name="healingAmount">The amount of health to gain, this value should be positive</param>
    public void Heal(int healingAmount)
    {
        // Heal us and update our heathbar
        currentHealth += healingAmount;
        UIManager.instance.UpdatePlayerHealthSlider((float)currentHealth / (float)maxHealth);
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    /// <summary>
    /// TakeDamage handles the functionality for taking damage
    /// </summary>
    /// <param name="damageAmount">The amount of damage to lose, this value should be positive</param>
    public void TakeDamage(int damageAmount)
    {
        // Hurt us and update our healthbar
        currentHealth -= damageAmount;

        UIManager.instance.UpdatePlayerHealthSlider((float)currentHealth / (float)maxHealth);

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }

    /// <summary>
    /// Handles all functionality related to when health reaches or goes below zero, should perform all necessary cleanup.
    /// </summary>
    public void Die()
    {
        // would be good to do some death animation here maybe
        // remove this object from the game
        Destroy(gameObject);
    }
}
