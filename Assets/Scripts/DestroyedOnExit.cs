﻿using UnityEngine;
using System.Collections;

// What occurs when the objected leaves the screen
public class DestroyedOnExit : MonoBehaviour
{

    // Called when the object leaves the viewport
    void OnBecameInvisible()
    {
        //if we are part of the pool return
        if (GetComponent<PoolObject>() != null)
        {
            PoolManager.instance.ReturnObjectToPool(gameObject);
        }

        else
        {
            // destroy the object
            Destroy(gameObject);
        }
    }
}
