﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// What happens when things hit
public class DamageOnCollision : DetectCollisionBase
{
    //how much damage to deal to respective object
    [SerializeField]
    private int damageToDeal;

    // What happens when we collide
    protected override void ProcessCollision(GameObject other)
    {
        // make sure the object has a health component and deal damage to it
        base.ProcessCollision(other);
        if (other.GetComponent<IHealth>() != null) 
        {
            other.GetComponent<IHealth>().TakeDamage(damageToDeal);
        } 
        else 
        {
            Debug.Log(other.name + " does not have an IHealth component");
        }
        //if we are part of the pool return to it
        if (GetComponent<PoolObject>() != null)
        {
            PoolManager.instance.ReturnObjectToPool(gameObject);
        }

        else
        {
            // destroy the object
            Destroy(gameObject);
        }
    }
}
