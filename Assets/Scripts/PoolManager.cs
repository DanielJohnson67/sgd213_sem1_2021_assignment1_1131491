using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager : MonoBehaviour
{

	public static PoolManager instance;
	
	//Set our poolinfo 
	[System.Serializable]
	public class PoolInfo
	{
		public string poolName;
		public GameObject prefab;
		public int poolSize;
		public bool fixedSize;
	}
	public PoolInfo[] poolInfo;
	//mapping of pool name vs list
	private Dictionary<string, Pool> poolDictionary = new Dictionary<string, Pool>();

	// Use this for initialization
	void Start()
	{
		//set instance
		instance = this;
		//create pools
		CreatePools();
	}

	//create the pools
	private void CreatePools()
	{
		foreach (PoolInfo currentPoolInfo in poolInfo)
		{
			Pool pool = new Pool(currentPoolInfo.poolName, currentPoolInfo.prefab,
								 currentPoolInfo.poolSize, currentPoolInfo.fixedSize);

			Debug.Log("Creating pool: " + currentPoolInfo.poolName);
			//add to mapping dict
			poolDictionary[currentPoolInfo.poolName] = pool;
		}
	}


	/* Returns an available object from the pool 
	OR 
	null in case the pool does not have any object available & can grow size is false.
	*/
	public GameObject GetObjectFromPool(string poolName, Vector3 position, Quaternion rotation)
	{
		GameObject result = null;

		if (poolDictionary.ContainsKey(poolName))
		{
			Pool pool = poolDictionary[poolName];
			result = pool.NextAvailableObject(position, rotation);
			//scenario when no available object is found in pool
			if (result == null)
			{
				Debug.LogWarning("No object available in pool. " + poolName);
			}

		}
		else
		{
			Debug.LogError("Invalid pool name " + poolName);
		}

		return result;
	}
	//send the object back to the pool
	public void ReturnObjectToPool(GameObject go)
	{
		PoolObject po = go.GetComponent<PoolObject>();
		if (po == null)
		{
			Debug.LogWarning("object not a pooled instance: " + go.name);
		}
		else
		{
			if (poolDictionary.ContainsKey(po.poolName))
			{
				Pool pool = poolDictionary[po.poolName];
				pool.ReturnObjectToPool(po);
			}
			else
			{
				Debug.LogWarning("No pool available with name: " + po.poolName);
			}
		}
	}
}
