﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// EnemyMovement is inheriting from our EngineBase movement and we can specify which parts we want to change in the child class 
public class EnemyMovement : EngineBase
{
    public override void Accelerate(Vector2 direction)
    {
        // Accelerate in the given direction
        base.Accelerate(direction);
    }
}
