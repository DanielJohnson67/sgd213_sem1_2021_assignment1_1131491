﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Pickup is an object aimed at passing weapon functionality to player objects. Depending on
// the specified weaponType, the Pickup will tell the player object what object it should now use as it's weapon.
public class Pickup : MonoBehaviour
{   
    // make weapon type a pseudo-public variable
    [SerializeField]
    public WeaponType weaponType;
    // when we trigger with a collider
    void OnTriggerEnter2D(Collider2D col)
    {
        // if the game object is the player 
        if (col.gameObject.tag == "Player")
        {
            // set the player to the game object
            GameObject player = col.gameObject;
            // trigger the handlePlayerPickup code
            HandlePlayerPickup(player);
        }
    }
    // what happens when it collides with the player
    void OnCollisionEnter2D(Collision2D col)
    {
        // if the gameobject is the player
        if (col.gameObject.tag == "Player")
        {
            // set the player to the game object
            GameObject player = col.gameObject;
            // trigger the handlePlayerPickup code
            HandlePlayerPickup(player);
        }
    }
    // HandlePlayerPickup handles all of the actions after a player has been collided with.
    // It grabs the IWeapon component from the player, transfers all information to a new IWeapon (based on the provided weaponType).
    private void HandlePlayerPickup(GameObject player)
    {
        // get the playerInput from the player
        PlayerInput playerInput = player.GetComponent<PlayerInput>();
        // handle a case where the player doesnt have a PlayerInput
        if (playerInput == null) {
            Debug.LogError("Player doesn't have a PlayerInput component.");
            return;
        } else {
            // tell the playerInput to SwapWeapon based on our weaponType
            playerInput.SwapWeapon(weaponType);
        }
    }

}

public enum WeaponType { machineGun, tripleShot }
