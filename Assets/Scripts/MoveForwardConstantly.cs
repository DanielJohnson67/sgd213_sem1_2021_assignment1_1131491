﻿using UnityEngine;
using System.Collections;
using System;
// script for moving forward constantly
public class MoveForwardConstantly : MonoBehaviour
{
    // make a pseudo public variable for acceleration
    [SerializeField]
    private float acceleration = 100f;

    // make a pseudo public variable for initial velocity
    [SerializeField]
    private float initialVelocity = 10f;

    //make a pseudo public variable for movement
    [SerializeField]
    private Vector2 movementDirection = Vector2.up;

    // make a variable for ourRigidBody
    private Rigidbody2D ourRigidbody;
    
    // Because we're in a Pool, and Start and Awake only ever get called once, we'll use OnEnable instead!
    // @TODO: Verify that we can't GetComponent<Rigidbody2D> in Start or Awake instead;
    public void OnEnable()
    {
        // get the component of our rigidbody
        ourRigidbody = GetComponent<Rigidbody2D>();

        // make our rigidbody's velociity equal to our vector multiplied by our initial velocity
        ourRigidbody.velocity = movementDirection * initialVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        // calculate the force we need to add by multiplying our vector by our acceleration by our current time step
        Vector2 forceToAdd = movementDirection * acceleration * Time.deltaTime;
        // add this force to our body
        ourRigidbody.AddForce(forceToAdd);
    }
}


