﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// PlayerInput handles all of the player specific input behaviour, and passes the input information to the appropriate scripts.
public class PlayerInput : MonoBehaviour
{
    // local references
    private EngineBase engine;

    private WeaponBase weapon;
    public WeaponBase Weapon
    {
        get
        {
            return weapon;
        }

        set
        {
            weapon = value;
        }
    }
    // when we start
    void Start()
    {
        // set the player movement component
        engine = GetComponent<EngineBase>();

        // set the player weapon component
        weapon = GetComponent<WeaponBase>();
    }
    //every frame
    void Update()
    {
        // read our horizontal input axis
        float horizontalInput = Input.GetAxis("Horizontal");
        // if movement input is not zero
        if (horizontalInput != 0.0f)
        {
            // ensure our playerMovementScript is populated to avoid errors
            if (engine != null)
            {
                // pass our movement input to our playerMovementScript
                engine.Accelerate(horizontalInput * Vector2.right);
            }
        }

        // if we press the Fire1 button
        if (Input.GetButton("Fire1"))
        {
            // if our shootingScript is populated
            if (weapon != null)
            {
                // tell shootingScript to shoot
                weapon.Shoot();
            }
        }
    }
    // SwapWeapon handles creating a new WeaponBase component based on the given weaponType. This
    // will popluate the newWeapon's controls and remove the existing weapon ready for usage.
    public void SwapWeapon(WeaponType weaponType)
    {
        // make a new weapon dependent on the weaponType
        WeaponBase newWeapon = null;
        switch (weaponType)
        {
            case WeaponType.machineGun:
                newWeapon = gameObject.AddComponent<WeaponMachineGun>();
                break;
            case WeaponType.tripleShot:
                newWeapon = gameObject.AddComponent<WeaponTripleShot>();
                break;
        }
        // update the data of our newWeapon with that of our current weapon
        newWeapon.UpdateWeaponControls(weapon);
        // remove the old weapon
        Destroy(weapon);
        // set our current weapon to be the newWeapon
        weapon = newWeapon;
    }
}
