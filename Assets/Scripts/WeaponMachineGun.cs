﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//our Weapon that is inherting from our WeaponBase script
public class WeaponMachineGun : WeaponBase
{

    // Shoot will spawn a new bullet, provided enough time has passed compared to our fireDelay.
    public override void Shoot()
    {
        // get the current time
        float currentTime = Time.time;

        // if enough time has passed since our last shot compared to our fireDelay, spawn our bullet
        if (currentTime - lastFiredTime > fireDelay)
        {
         
            // is the player firing if so grab our bullet from the pool
           if (gameObject.tag == "Player")
            {
                GameObject bullet = PoolManager.instance.GetObjectFromPool("Bullets", transform.position, transform.rotation);

                bullet.SetActive(true);

                // update our shooting state
                lastFiredTime = currentTime;

            }
           // is the enemy firing if so grab the enemies bullets from the pool
           if (gameObject.tag == "Enemy")
            {
                GameObject bullet = PoolManager.instance.GetObjectFromPool("BulletEnemy", transform.position, transform.rotation);

                bullet.SetActive(true);

                // update our shooting state
                lastFiredTime = currentTime;
            }
        }
    }
}
