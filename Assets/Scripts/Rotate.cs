﻿using UnityEngine;
using System.Collections;
// script to rotate by a maximumSpinSpeed
public class Rotate : MonoBehaviour
{
    // set our inital maximumSpinSpeed value to a 200 float
    public float maximumSpinSpeed = 200;

    // rotate the object in a range from negative max spin speed to positive max spin speed
    void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-maximumSpinSpeed, maximumSpinSpeed);
    }
}
