﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//our Weapon that is inherting from our WeaponBase script
public class WeaponTripleShot : WeaponBase 
{
    // Shoot will spawn a three bullets, provided enough time has passed compared to our fireDelay.
    public override void Shoot() 
    {
        // get the current time
        float currentTime = Time.time;

        //print("Shoot triple shot");
        // if enough time has passed since our last shot compared to our fireDelay, spawn our bullet
        if (currentTime - lastFiredTime > fireDelay) 
        {
            //is it the player firing?
            if (gameObject.tag == "Player")
            {
                float x = -0.5f;
                // create 3 bullets
                for (int i = 0; i < 3; i++)
                {
                    // create our bullet
                    GameObject bullet = PoolManager.instance.GetObjectFromPool("Bullets", transform.position, transform.rotation);

                    bullet.SetActive(true);

                    // set their direction
                    bullet.GetComponent<MoveConstantly>().Direction = new Vector2(x + 0.5f * i, 0.5f);
                }

                // update our shooting state
                lastFiredTime = currentTime;
            }
            //is it the enemy firing
            if (gameObject.tag == "Enemy")
            {
                float x = -0.5f;
                // create 3 bullets
                for (int i = 0; i < 3; i++)
                {
                    GameObject bullet = PoolManager.instance.GetObjectFromPool("BulletEnemy", transform.position, transform.rotation);

                    bullet.SetActive(true);

                    bullet.GetComponent<MoveConstantly>().Direction = new Vector2(x + 0.5f * i, -0.5f);
                }
                // update our shooting state
                lastFiredTime = currentTime;

            }
    }
}


}
