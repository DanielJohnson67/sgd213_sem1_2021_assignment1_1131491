﻿using UnityEngine;
using System.Collections;

// MoveConstantly gives an object the ability to continuously move based on the specified direction, acceleration and initialVelocity variables.
public class MoveConstantly : MonoBehaviour
{
    // make a pseudo public variable for acceleration
    [SerializeField]
    private float acceleration = 100f;
    // make a pseudo public variable for initial velocity
    [SerializeField]
    private float initialVelocity = 10f;

    [SerializeField]
    // our direction to move in
    private Vector2 direction = new Vector2(0, 1);


    // Direction provides access to the direction variable used to direct the movement of our object.
    // It is expected that when setting the direction, the provided Vector2 is a unit vector. If not, it will be automatically normalised.
    public Vector2 Direction 
    {
        get 
        {
            // return the direction
            return direction;
        }
        set 
        {
            // if our magnitude is 1
            if (value.magnitude == 1) 
            {
                // our direction is the value
                direction = value;
                // else if its anything but 1
            } else 
            {
                // normalise our direction
                direction = value.normalized;
            }
        }
    }
    // local references
    private Rigidbody2D ourRigidbody;
    // when we start
    void OnEnable()
    {
        // get the component of our rigidbody and assign it to a variable
        ourRigidbody = GetComponent<Rigidbody2D>();
        // the velocity of our rigid body is our direction and initial velocity
        ourRigidbody.velocity = direction * initialVelocity;
    }

    void Update()
    {
        // calculate our force to add, based on our provided direction, acceleration and delta time
        Vector2 forceToAdd = direction * acceleration * Time.deltaTime;
        // add our forceToAdd to ourRigidbody
        ourRigidbody.AddForce(forceToAdd);
    }
}
